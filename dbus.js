const DBus = require('dbus');
const sqlite = require("./sqlite.js");
const app = require('./core.js');
const helper = require('./helper.js');
const dbusNative = require('dbus-native');
const moment = require('moment')
var open = require('opn');

        //const { exec } = require('child_process');
function registerHamsterService(){
    let service = DBus.registerService('session','org.gnome.Hamster');
    let obj = service.createObject('/org/gnome/Hamster');
    let hamsterInterface = obj.createInterface('org.gnome.Hamster');
    return hamsterInterface
}


function startHamsterService(){

  console.log('Starting Dbus service ...');
    this.hamsterInterface = registerHamsterService();
    this.hamsterInterface = addHamsterMethods(this.hamsterInterface);


    let WindowBus = dbusNative.sessionBus();
    WindowBus.requestName('org.gnome.Hamster.WindowServer', 0);
    WindowBus.exportInterface(addHamsterWindowMethods, '/org/gnome/Hamster/WindowServer', hamsterWindowService);
    console.log('dbus interfaces are running...');

    this.hamsterInterface.emitSignal('FactsChanged');

    this.emitFactsChanged = function() {
        this.hamsterInterface.emitSignal('FactsChanged')
        return this.hamsterInterface
    }/*in: [DBus.Define(String),DBus.Define(Boolean)]*/
    console.log('devices have been alerted...');

}



let hamsterWindowService = {
    name: 'org.gnome.Hamster.WindowServer',
    methods: {
        edit: ['v','b'],
        overview: ['v','b'],
    }
};



let addHamsterWindowMethods = {
    edit: function(v) {
        let id = v[1][0]
        console.log('editing requested via dbus')
        open('http://localhost:8080/edit?id='+id);
        return true;
    },

    overview: function(v){
        open('http://localhost:8080/');
        return true;
    }
};



function addHamsterMethods(iface1){/*in: ['v'],*/
    iface1.addMethod('GetTodaysFacts', {out: {type:'a(iiissisasii)'} },
        (callback) => {
            app.getTodaySessions( (todaySession) => {
                let formated = Array();

                for (var i in todaySession) {
                    let delta,stop,start ;

                    if(todaySession[i].delta <= 0 ){
                        stop = 0;
                        delta =  moment().unix() - todaySession[i].start;

                    }else{
                        delta = todaySession[i].delta
                        stop = todaySession[i].stop + (moment(todaySession[i].stop*1000).utcOffset()*60)
                        //stop = todaySession[i].stop

                    }
                    start = todaySession[i].start   + (moment(todaySession[i].start*1000).utcOffset()*60)  //        startTime
                    //start = todaySession[i].start
                    formated.push([
                        todaySession[i].id,               //        name: fact[4],
                        parseInt(start),  //        startTime
                        parseInt(stop),             //        endTime: || null
                        '',                               //        description
                        todaySession[i].taskName,                         //        activity Name
                        todaySession[i].task,             //        category ID
                        todaySession[i].projectName,                      //        category Name
                        [],                               //        tags: fact[7],
                        parseInt(todaySession[i].start),  //        date: UTCToLocal(fact[8] * 1000),
                        delta ,            //        delta: Math.floor(fact[9] / 60), // minutes
                    ])
                }
                callback(null, formated);
            })
        });



    iface1.addMethod('StopTracking', {
        in: [DBus.Define(Number)],
        out: DBus.Define(Boolean)
    }, function(a, callback) { // a is UTC and UNIX timestamp

        let running = sqlite.getOpenSessions();
        for( let i in running ){
            let gwStopTime =  ( a - (moment(a*1000).utcOffset()*60) )
            console.log( "Stop " + running[i].id + " at (UTC)" + gwStopTime+". Raw data is(local time) "+ a);
            gwStopTime = parseInt(gwStopTime);
            sqlite.updateSession(running[i].id,{stop:gwStopTime})
        }
        iface1.emitSignal('FactsChanged')
        callback(null, true);
    });


    iface1.addMethod('AddFact', {
        in: [DBus.Define(String),DBus.Define(Number),DBus.Define(Number),DBus.Define(Boolean)],
        out: DBus.Define(Number)
    }, function(fact,start_time,end_time,temporary, callback) {

        let taskAndProject = fact.split('@');
        if ( taskAndProject.length === 1 ){
            taskAndProject[1] = 'default';
        }

        app.newSession({
            'start': moment().unix(),
            'task': taskAndProject[0] ,
            'project': taskAndProject[1]
        },(id) => {
            if( id ){
                console.log( 'Session ID '+id+' have started at now' )
            }
        });
        iface1.emitSignal('FactsChanged')
        callback(null, true);
    });


    iface1.addMethod('GetActivities', {
        in: [DBus.Define(String)],
        out: {type:'a(ss)'}
    }, function(a, callback) {
        let response = Array();
        let list = sqlite.getAlltasks();
        const regex = new RegExp( '^' + a + "");
        for( i in list){
            if(list[i].name.match(regex)){

                let projects = sqlite.getSessionsOfTaskProject(list[i].id);

                for ( let ii in projects  ){
                    let project = sqlite.getProjectId(projects[ii].project);
                    response.push([list[i].name, project]);
                }
            }
        }
        callback(null, response);
    });


    iface1.addMethod('GetCategories', {
        out: {type:'a(is)'}
    }, function( callback) {
        let response = Array();
        let list = sqlite.getAllProjects();

        for( let i in list  ){
            response.push([list[i].id,list[i].name ])
        }

        callback(null, response);
    });
    iface1.update();

    iface1.addSignal('update', {});
    iface1.addSignal('FactsChanged', {});
    iface1.addSignal('ActivitiesChanged', {});
    iface1.addSignal('TagsChanged', {});

    iface1.update();
    return iface1;
}


let emitFactsChanged = () => {
//console.log(this.hamsterInterfacee)
    this.hamsterInterface.emitSignal('FactsChanged');

}



/* @deprecated Use startHamsterService instead */
const run = () => {
    let service = DBus.registerService('session','org.gnome.Hamster');
    let obj = service.createObject('/org/gnome/Hamster');
    let iface1= obj.createInterface('org.gnome.Hamster');
    // let iface1 = registerHamsterService();

    iface1.addMethod('GetTodaysFacts', {out: {type:'a(iiissisasii)'} },
        function(callback) {
            app.getTodaySessions( (todaySession) => {
                let formated = Array();
                for (var i in todaySession) {
console.log("today");
console.log(todaySession);
                    if(todaySession[i].stop == -1 ){
                        todaySession[i].stop = 0;
                        todaySession[i].delta =  helper.anyDateToTimestamp('now') - todaySession[i].start;
                    }else{
                        todaySession[i].stop = helper.addTimeZoneOffset(todaySession[i].stop)
                    }


                    formated.push([
                            todaySession[i].id,               //        name: fact[4],
                            parseInt(helper.addTimeZoneOffset(todaySession[i].start)),  //        startTime
                            todaySession[i].stop,             //        endTime: || null
                            '',                               //        description
                            todaySession[i].taskName,         //        activity Name
                            todaySession[i].task,             //        category ID
                            todaySession[i].projectName,      //        category Name
                            [],                               //        tags: fact[7],
                            parseInt(helper.addTimeZoneOffset(todaySession[i].start)),  //        date: UTCToLocal(fact[8] * 1000),
                            todaySession[i].delta,            //        delta: Math.floor(fact[9] / 60), // minutes
                    ])
                }
                callback(null, formated);
            })
        });



    iface1.addMethod('StopTracking', {
        in: [DBus.Define(Number)],
        out: DBus.Define(Boolean)
    }, function(a, callback) { // a is UTC and UNIX timestamp

        let running = sqlite.getOpenSessions();
        for( let i in running ){
            let gwStopTime = a;
            console.log( "Stop LEGACY ID " + running[i].id + " at " + gwStopTime);
            console.log( "GW time  " + gwStopTime+ " ||| A time " + a);


            sqlite.updateSession(running[i].id,{stop:gwStopTime})
        }
        iface1.emitSignal('FactsChanged')
        callback(null, true);
    });



    iface1.addMethod('AddFact', {
        in: [DBus.Define(String),DBus.Define(Number),DBus.Define(Number),DBus.Define(Boolean)],
        out: DBus.Define(Number)
    }, function(fact,start_time,end_time,temporary, callback) {

        let taskAndProject = fact.split('@');
        if ( taskAndProject.length === 1 ){
            taskAndProject[1] = 'default';
        }

        app.newSession({
            'start': helper.anyDateToTimestamp('now'),
            'task': taskAndProject[0] ,
            'project': taskAndProject[1]
        },(id) => {
            if( id ){
                console.log( 'Session ID '+id+' have started at '+ helper.anyDateToTimestamp('now') )
            }
        });
        iface1.emitSignal('FactsChanged')
        callback(null, true);
    });


    iface1.addMethod('GetActivities', {
        in: [DBus.Define(String)],
        out: {type:'a(ss)'}
    }, function(a, callback) {
        let response = Array();
        let list = sqlite.getAlltasks();
        const regex = new RegExp( '^' + a + "");
        for( i in list){
            if(list[i].name.match(regex)){

                let projects = sqlite.getSessionsOfTaskProject(list[i].id);

                for ( let ii in projects  ){
                    let project = sqlite.getProjectId(projects[ii].project);
                    response.push([list[i].name, project]);
                }
            }
        }
        callback(null, response);
    });



    iface1.addMethod('GetCategories', {
        out: {type:'a(is)'}
    }, function( callback) {
        let response = Array();
        let list = sqlite.getAllProjects();

        for( let i in list  ){
            response.push([list[i].id,list[i].name ])
        }

        callback(null, response);
    });
    iface1.update();


// Signals //

    iface1.addSignal('update', {});
    iface1.addSignal('FactsChanged', {});
    iface1.addSignal('ActivitiesChanged', {});
    iface1.addSignal('TagsChanged', {});

    iface1.update();

    windowIface.update();
};

module.exports = {
    startHamsterService,
    emitFactsChanged
};
