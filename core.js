const sqlite = require('./sqlite.js')
const helper = require('./helper.js')
const moment = require('moment')

const newTask = (name, callback) => {
  if (typeof name === 'string') {
    let id = sqlite.createTask(name)
    callback(id)
  }
}

const newProject = (name, callback) => {
  if (typeof name === 'string') {
    let id = sqlite.createProject(name)
    callback(id)
  }
}

const newSession = (data, callback) => {
  let openSessions = sqlite.getOpenSessions()
  if (openSessions) {
    for (let i in openSessions) {
      stopSession(openSessions[i].id, 'now', () => {})
    }
  }

  if (data.start) {
    let timestamp = helper.anyDateToTimestamp(data.start)
    data.start = timestamp
  }

  if (data.stop) {
    let timestamp2 = helper.anyDateToTimestamp(data.stop)
    if (typeof timestamp2 === 'number') {
      if (timestamp2 > 0) {
        data.stop = timestamp2
      } else {
        data.stop = -1
      }
    } else {
      // TODO ERROR invalid time format
    }
  } else {

  }

  if (data.task) {
    let task = data.task
    if (typeof task === 'number') { // if it's an ID
      if (!sqlite.getNameOfTaskId(task)) { // if ID don't exist
        task = 1 // fallback to default
      }
    } else { // if it's a name
      if (!sqlite.getIdOfTaskName(task.trim())) { // if name don't exist, create one
        task = sqlite.createTask(task.trim())
      } else {
        task = sqlite.getTaskName(task.trim())
      }
    }

    if (task !== undefined) { // if we have a valid ID
      data.task = task
    }
  }

  if (data.project) {
    let project = data.project
    if (typeof project === 'number') {
      if (!sqlite.getProjectID(project)) { // if ID don't exist
        project = 1 // fallback to default
      }
    } else {
      if (!sqlite.getProjectName(project.trim())) { // if name don't exist, create one
        project = sqlite.createProject(project.trim())
      } else {
        project = sqlite.getProjectName(project.trim())
      }
    }

    if (project !== undefined) { // if we have a valid ID
      data.project = project
    }
  }

  if (data.tags) {
    // TODO
  }

  callback(sqlite.createSession(data))
}

const getTodaySessions = (callback) => {
  let ids = sqlite.getSessionsStartedBetween(moment.utc().startOf('day'), moment.utc().endOf('day'), true)
  callback(ids)
}

const getSessionsBetween = (begining, end, callback) => {
  let ids = sqlite.getSessionsStartedBetween(begining, end, true)
  callback(ids)
}

var stopSession = (id, stopTime, callback) => {
  if (!id) {
    getRunningSessions()
    // todo stop all running sessions
  }

  id = parseInt(id)
  if (typeof id === 'number') {
    let session = sqlite.getSessionId(id)
    if (session && session.stop === -1) {
      let data = { stop: helper.anyDateToTimestamp(stopTime) }
      callback(sqlite.updateSession(id, data))
    } else {
      callback(-1)
    }
  } else {
    callback(-1)
  }
}

const updateSession = (id, data, callback) => {
  if (isNaN(id)) {
    callback(-1)
  } else if (!sqlite.getSessionId(id, false)) {
    callback(-1)
  }
  data.stop = helper.userInputToTimestamp(data.stop)
  data.start = helper.userInputToTimestamp(data.start)
  let taskId = sqlite.getIdOfTaskName(data.task)
  if (taskId === false) {
    taskId = sqlite.createTask(data.task)
  }
  data.task = taskId

  let projectId = sqlite.getIdOfProjectName(data.project)
  if (projectId === false) {
    projectId = sqlite.createProject(data.project)
  }
  data.project = projectId

  callback(sqlite.updateSession(id, data))
}

const getSession = (id, callback) => {
  if (!isNaN(id) && id > 0) {
    let rep = sqlite.getSessionId(id)
    callback(rep)
  } else {
    callback(false)
  }
}

const autocompleteProject = (needle, callback) => {
  let list = sqlite.getAllProjects()
  let results = []
  for (let i in list) {
    if (list[i].name.match('/^' + needle + '*/g')) {
      results.push(list[i].name)
    }
  }
  callback(results)
}

const autocompleteTask = (needle, callback) => {
  let list = sqlite.getAlltasks()
  let results = []
  for (let i in list) {
    if (list[i].name.match('/^' + needle + '*/g')) {
      results.push(list[i].name)
    }
  }
  callback(results)
}

const getRunningSessions = (callback) => {
  callback(sqlite.getOpenSessions())
}

const getSessionsOfProject = (name, callback) => {
  let r = sqlite.getSessionsOfTaskProject('*', sqlite.getProjectName(name))
  callback(r)
}

const getSessionsOfTask = (name, callback) => {
  let r = sqlite.getSessionsOfTaskProject(sqlite.getTaskName(name), '*')
  callback(r)
}

const getSessionsOfTaskAtProject = (task, project, callback) => {
  let r = sqlite.getSessionsOfTaskProject(sqlite.getTaskName(task), sqlite.getProjectName(project))
  callback(r)
}

const listProjects = (callback) => {
  callback(sqlite.getAllProjects())
}

const listTasks = (callback) => {
  callback(sqlite.getAlltasks())
}

const getTaskProjectOfSessionId = (id, callback) => {
  let session = sqlite.getSessionId(id)
  if (session) {
    callback(sqlite.getTaskId(session.task) + '@' + sqlite.getProjectId(session.project))
  } else {
    callback('')
  }
}

module.exports = {
  newTask,
  newProject,
  newSession,
  getTodaySessions,
  stopSession,
  autocompleteProject,
  autocompleteTask,
  getSession,
  getSessionsBetween,
  getRunningSessions,
  getSessionsOfProject,
  getSessionsOfTask,
  getSessionsOfTaskAtProject,
  listProjects,
  listTasks,
  getTaskProjectOfSessionId,
  updateSession
}
