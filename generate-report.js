const os = require('os');
const fs = require('fs');
const moment = require('moment')
const crypto = require('crypto');
const opn = require('opn');
const config = require('./config.js');
const sqlite = require('./sqlite.js');

let nonce = crypto.randomBytes(8).toString('hex');
let tmpReportLocation = '/tmp/'+nonce+'_report.html';
let rawsessions = sqlite.getAllSessions();
let formatedData = [] ;


for (i in rawsessions){
    formatedData[i] = {
      'activity':         rawsessions[i].taskName,     //=> $activity['name'],
      'category':         rawsessions[i].projectName,   //=> $category['name'],
      'activity_id':      rawsessions[i].task, //=> $row['activity_id'],
      'tags':       '',   //=> array(),
      'description':'',   //=> $row['description'],
      'start_time':       moment.unix(rawsessions[i].start).format('YYYY-MM-DD HH:mm'),  //=> $row['start_time'],
      'tag':         '',  //=> $tagName,
      'end_time':         rawsessions[i].stop,  //=> $row['end_time'],
      'delta':            (rawsessions[i].stop - rawsessions[i].start)/60,   //=> $delta,
      'id':               rawsessions[i].id,     //=> $row['id'],
      'date':             moment.unix(rawsessions[i].start).format('YYYY-MM-DD'),    //=> $row['start_time'],
      'name':             rawsessions[i].taskName,     //=> $activity['name']
  }
}

let bigJson = JSON.stringify(formatedData) ;
let originalReport = fs.readFileSync('./report-src/report.html','utf8');
let report = originalReport.replace('<![CDATA[]]>', '<![CDATA['+bigJson+']]>');

let err = fs.writeFileSync(tmpReportLocation, report, 'utf8');
fs.chmodSync(tmpReportLocation, '600');
console.log(tmpReportLocation)
opn(tmpReportLocation);
