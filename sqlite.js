const Database = require('better-sqlite3');
const os = require('os');
const fs = require('fs');
const config = require('./config.js');

/**
 * Model of a default session structure.
 * @private Object
 */
const sessionModel = {
    'id': '-1',
    'start': '-1',
    'stop': '-1',
    'delta': '-1',
    'task': '0',
    'comment': '',
    'user': '0',
    'tags': []
};

/**
 *  @constructor database connection.
 *  @private
 *  @type object
 */
const db = connect(config.sqlitePath);

/**
 * Create or connect to the database file provided as argument and return the handle.
 * @function connect
 * @param {string} dbFile - relative or absolute path to the sqlite3 file. (Missing folders and files will be created ) test env will ignore this parameter .
 * @private
 */
function connect(dbFile) {
    if (process.env.NODE_ENV === 'test') {
        dbFile = ':memory:';
    }else if(!dbFile){
        dbFile = os.homedir() + '/.local/share/gerboise/gerboise.sqlite3'
    }

    if (!fs.existsSync(dbFile)) {
        let path = require('path').dirname(dbFile);
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path);
        }
        var newDb = new Database(dbFile);
        newDb.exec('CREATE TABLE IF NOT EXISTS sessions (id INTEGER PRIMARY KEY AUTOINCREMENT, start TEXT, stop TEXT, task INT, project TEXT, comment TEXT, user INT, tags TEXT )');
        newDb.exec('CREATE TABLE IF NOT EXISTS tasks (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)');
        newDb.exec('CREATE TABLE IF NOT EXISTS projects (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)');
        newDb.exec('CREATE TABLE IF NOT EXISTS tags (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)');
        newDb.exec("INSERT INTO projects ('name') VALUES ('default')");
        newDb.exec("INSERT INTO tasks ('name') VALUES ('default')");
        return newDb;
    } else {
        return new Database(dbFile);
    }
}

/**
 * Count the amound of tables in the sqlite3 file. (only used for test unit)
 * @function countTables
 * @returns {object} the count of table found
 */
function countTables() {
    return db.prepare("SELECT COUNT(*) FROM sqlite_master WHERE type = 'table'").all();
}

/**
 * Add a new project.
 * @function createProject
 * @param {string} name - new project name
 * @returns {boolean,int} - the new project Id or false.
 */
function createProject(name) {
    if (!getIdOfProjectName(name)) {
        db.exec("INSERT INTO projects ('name') VALUES ('" + name + "')");
        return getIdOfProjectName(name);
    } else {
        return false;
    }
}

/**
 * Create a task if does not already exist.
 * @function createTask
 * @param {string} name
 * @returns {int} Id
 */
function createTask(name) {
    if (!getIdOfTaskName(name)) {
        db.exec("INSERT INTO tasks ('name') VALUES ('" + name + "')");
        return getIdOfTaskName(name);
    } else {
        return false;
    }
}

/**
 * Create a new session entry based on the session model + overrided by parameter data.
 * @function createSession
 * @param {Object} sessionData - an object of data to add over the default session model.
 * @returns {int} the created session Id
 */
function createSession(sessionData) {
    if (sessionData.start) {
        sessionModel.start = sessionData.start
    }
    if (sessionData.stop) {
        sessionModel.stop = sessionData.stop
    }
    if (sessionData.task) {
        sessionModel.task = sessionData.task
    }
    if (sessionData.project) {
        sessionModel.project = sessionData.project
    }
    if (sessionData.comment) {
        sessionModel.comment = sessionData.comment
    }
    if (sessionData.user) {
        sessionModel.user = sessionData.user
    }
    if (sessionData.tags) {
        sessionModel.tags = sessionData.tags
    }

    let sql = db.prepare("INSERT INTO sessions ('start', 'stop',  'task', 'project', 'comment', 'user', 'tags') VALUES ('" +
        sessionModel.start + "','" +
        sessionModel.stop + "','" +
        sessionModel.task + "','" +
        sessionModel.project + "','" +
        sessionModel.comment + "','" +
        sessionModel.user + "','" +
        sessionModel.tags + "')");
    return sql.run().lastInsertRowid;
}

/**
 * TODO - not implemented
 */
function createTag(tagName) {
    return false;
}

/**
 * Get the list of all the projects.
 * @function getAllProjects
 * @returns {array} list of objects containing id and name of all the projects.
 */
function getAllProjects() {
    return db.prepare('SELECT * FROM projects;').all();
}


/**
 * This is deprecated due to confusing function name. plz use getNameOfProjectId instead
 * @function getProjectId
 * @param id
 * @returns {Object}
 * @deprecated due to confusing function name. plz use getNameOfProjectId()
 */
function getProjectId(id) {
    return getNameOfProjectId(id);
}


/**
 * Get the name of a submitted project id
 * @function getNameOfProjectId
 * @param {int} id of the project
 * @returns {string} name of the project.
 */
function getNameOfProjectId(id) {
    let row = db.prepare('SELECT name FROM projects WHERE id = ? ;').get(id);
    if (row) {
        return row.name;
    }
}


/**
 * This is deprecated due to confusing function name. plz use getIdOfProjectName instead
 * @function getProjectName
 * @param name
 * @returns {Object}
 * @deprecated due to confusing function name. plz use getIdOfProjectName()
 */
function getProjectName(name) {
    return getIdOfProjectName(name);
}


/**
 * Get the id of a project id
 * @function getIdOfProjectName
 * @param {string} name name of the project
 * @returns {int,boolean} the id of the project of false if not found
 */
function getIdOfProjectName(name) {
    let row = db.prepare('SELECT id FROM projects WHERE name = ? ;').get(name);
    if (row) {
        return row.id;
    } else {
        return false;
    }
}

/**
 * get all the tasks
 * @function getAlltasks
 * @returns {array} a list of objects containing id and name of all the tasks.
 */
function getAlltasks() {
    return db.prepare('SELECT * FROM tasks;').all();
}


/**
 * This is deprecated due to confusing function name. plz use getNameOfTaskId instead
 * @function getTaskId
 * @param id
 * @returns {Object}
 * @deprecated due to confusing function name. plz use getNameOfTaskId()
 */
function getTaskId(id) {
    return getNameOfTaskId(id);
}

/**
 * Get the name of a provided task Id
 * @function getNameOfTaskId
 * @param {int} id of task
 * @returns {string,boolean} the name of the task, false if not found.
 */
function getNameOfTaskId(id) {
    let row = db.prepare('SELECT name FROM tasks WHERE id = ? ;').get(id);
    if (row) {
        return row.name;
    } else {
        return false;
    }
}


/**
 * This is deprecated due to confusing function name. plz use getIdOfTaskName instead
 * @function getTaskName
 * @param name
 * @returns {Object}
 * @deprecated due to confusing function name. plz use getIdOfTaskName()
 */
function getTaskName(name) {
    return getIdOfTaskName(name);
}

/**
 * Get the id of a provided task name
 * @function getIdOfTaskName
 * @param {string} name of task
 * @returns {int,boolean} the id of the task, false if not found.
 */
function getIdOfTaskName(name) {
    let row = db.prepare('SELECT id FROM tasks WHERE name = ? ;').get(name);
    if (row) {
        return row.id;
    } else {
        return false;
    }
}


/**
 * Get a dump of all the sessions registered in the database.
 * @function getAllSessions
 * @param {boolean} generateHumanReadable inject a series of generated methods in the results to ease the gui parsing.
 * @returns {array} an array of objects containing the sessions
 */
function getAllSessions(generateHumanReadable = true) {
    let sessions = db.prepare('SELECT * FROM sessions;').all();
    if (generateHumanReadable) {
        for (let i in sessions) {
            sessions[i] = addHumanReadableToSession(sessions[i]);
        }
    }
    return sessions;
}

/**
 * Get all the open sessions (stop time === -1)
 * @function getOpenSessions
 * @param {boolean} generateHumanReadable inject a series of generated methods in the results to ease the gui parsing.
 * @returns {array} an array of objects containing the found sessions.
 */
function getOpenSessions(generateHumanReadable = true) {
    let sessions = db.prepare('SELECT * FROM sessions WHERE `stop` = "-1" ').all();
    if (generateHumanReadable) {
        for (let i in sessions) {
            sessions[i] = addHumanReadableToSession(sessions[i]);
        }
    }
    return sessions;
}


/**
 * Get all the sessions that have started between the 2 provided timestamps.
 * @function getSessionsStartedBetween
 * @param {boolean} generateHumanReadable inject a series of generated methods in the results to ease the gui parsing.
 * @param {int} start The starting timestamp.
 * @param {int} end The ending timestamp.
 * @returns {array} an array of objects containing the found sessions.
 */
function getSessionsStartedBetween(start, end, generateHumanReadable = true) {
    let sessions = db.prepare("SELECT * FROM sessions WHERE `start` BETWEEN " + start + " AND " + end ).all();
    if (generateHumanReadable) {
        for (let i in sessions) {
            sessions[i].stop = parseInt(sessions[i].stop)
            sessions[i].start = parseInt(sessions[i].start)
            sessions[i] = addHumanReadableToSession(sessions[i]);
        }
    }
    return sessions;
}

/**
 * Get a specific session.
 * @function getSessionId
 * @param {int} id the Id to query.
 * @returns {object} the found session.
 */
function getSessionId(id, generateHumanReadable = true) {
    let session  = db.prepare('SELECT * FROM sessions WHERE id = ? ;').get(id);
    if(generateHumanReadable ){
        return addHumanReadableToSession(session)
    }
    return session;
}

/**
 * Update an existing session
 * @function updateSession
 * @param {int} id
 * @param {Object} sessionData - an object of data to update.
 */
function updateSession(id, data) {
    let sql = 'UPDATE sessions SET     ';

    if (data.start) {
        sql += ' `start` = ' + data.start + ', ';
    }

    if (data.stop) {
        sql += ' `stop` = ' + data.stop + ', ';
    }

    if (data.task) {
        sql += ' `task` = ' + data.task + ', ';
    }

    if (data.project) {
        sql += ' `project` = ' + data.project + ', ';
    }

    if (data.comment) {
        sql += ' `comment` = "' + data.comment + '", ';
    }

    sql = sql.substring(0, sql.length - 2);

    sql += " WHERE `id` = '" + id + "';";

    db.exec(sql);
    //TODO return something plz.
}

/**
 * Update an existing session
 * @function updateSession
 * @param {int} id the target session Id
 * @param {Object} sessionData - an object of data to update.
 */
// will take: task id + project ID.  only task ID, only project ID (with `null` passed as task parameter)
function getSessionsOfTaskProject(task, project, generateHumanReadable = true) {
    let query = 'SELECT * FROM sessions WHERE';

    if (!isNaN(task)) {
        query += ' `task` = ' + task + ' ';
    }

    if (!isNaN(task) && !isNaN(project)) {
        query += ' AND ';
    }

    if (!isNaN(project)) {
        query += ' `project` = ' + project + ' ';
    }

    let sessions = db.prepare(query).all();
    if (generateHumanReadable) {
        for (let i in sessions) {
            sessions[i] = addHumanReadableToSession(sessions[i]);
        }
    }
    return sessions;
}

/**
 * Inject a series of generated methods in the session object to ease the recurrent parsing of raw.
 * @function addHumanReadableToSession
 * @private
 * @param {Object} session object.
 * @returns {Object} new and improved session object.
 */
function addHumanReadableToSession(session) {
    if (session.stop != -1) {
        session.delta = session.stop - session.start;
    } else {
        session.delta = -1;
    }
    session.taskName = getNameOfTaskId(session.task);
    session.projectName = getNameOfProjectId(session.project);
    session.taskProject = session.taskName + '@' + session.projectName;

    return session;
}


module.exports = {

    countTables, //For testing
    createProject,
    createTask,
    createSession,

    getAllProjects,
    getAlltasks,
    getAllSessions,
    getSessionsStartedBetween,
    getOpenSessions,

    getProjectId,
    getNameOfProjectId,
    getProjectName,
    getIdOfProjectName,
    getTaskId,
    getNameOfTaskId,
    getTaskName,
    getIdOfTaskName,
    getSessionId,
    getSessionsOfTaskProject,
    addHumanReadableToSession,
    updateSession,
};