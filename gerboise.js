require('dotenv').config()
const express = require('express')
const core = require('./core.js')
const dbus = require('./dbus')
const debug = require('debug')

// setup webserver
const app = express()
app.use(express.urlencoded())
app.use('/static', express.static('node_modules/jquery/dist/'))
app.use('/static', express.static('node_modules/bootstrap/dist/js/'))
app.use('/static', express.static('node_modules/bootswatch/dist/slate/'))
app.use('/static', express.static('node_modules/moment/min/'))
// setup debug
// let error = debug('webInterface')
let log = debug('webInterface')
log('Debug is enabled.')

// start dbus
// let hamsterInterface = new dbus.startHamsterService()
dbus.startHamsterService()

// web requests
app.get('/', function (req, res) {
  log('Main requested')
  res.sendFile('./views/main.html', { root: __dirname })
})

app.get('/edit', function (req, res) {
  log('Edit requested')
  res.sendFile('./views/edit.html', { root: __dirname })
})

app.get('/runningtask.json', function (req, res) {
  log('runningTask Json requested')
  core.getRunningSessions((session) => {
    if (typeof session[0] === 'object') {
      log(session[0])
      res.json(session[0])
    }
  })
})

app.get('/gettodaysessions.json', function (req, res) {
  log('runningTask Json requested')
  core.getTodaySessions((session) => {
    log(session)
    res.json(session)
  })
})

app.get('/getthismonthsessions.json', function (req, res) {
  log('getthismonthsessions Json requested')
  var date = new Date(); var y = date.getFullYear(); var m = date.getMonth()
  var firstDay = new Date(y, m, 1)
  var lastDay = new Date(y, m + 1, 0)
  log('First day of the month is : ' + firstDay)
  log('Last day of the month is  : ' + lastDay)
  core.getSessionsBetween(firstDay.getTime() / 1000, lastDay.getTime() / 1000, (session) => {
    log(session)
    res.json(session)
  })
})

app.get('/getSession.json', function (req, res) {
  log('getSession Json requested for session :' + req.param('id'))
  core.getSession(req.param('id'), (session) => {
    log(session)
    res.json(session)
  })
})

app.post('/updatesession', function (req, res) {
  log('Updatesession requested')
  log(req.body)

  core.getSession(req.body.taskId, (session) => {
    let data = {}
    data.stop = req.body.stop
    data.start = req.body.start
    data.task = req.body.task
    data.project = req.body.project

    if (session !== false) {
      core.updateSession(req.body.taskId, data, function () {
        res.json(session)
      })
    } else {
      core.newSession(data, (sessionId) => {
        log('NewSession id is ' + sessionId)
        res.json({ 'id': sessionId })
      })
    }
  })
})

app.listen(process.env.PORT || 8080, 'localhost')
