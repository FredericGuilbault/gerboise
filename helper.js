const moment = require('moment');



/**
 * @function userInputToTimestamp
 * Try to parse what the user have submited and turn it in a timestamp
 * ( this function does not change the submmited value to UTC )
 * @param {string,int} a date to parse
 * @returns {int,boolean} a timestamp
 */
 var userInputToTimestamp = (date) => {
console.log(date)
     // if it already look like a timestamp, do nothing
     if( ! isNaN(date)  && date > 0){
         return date;

     // if it's it's a negative value or NaN we assume it's the timestamp of a running task
} else if( typeof date === "number"     ){
             return -1

     // if the string `now` have been submitted. Change it for the localtime timestamp
     }else if (date === 'now') {
         return moment().utc().unix();

     // if string match HH:MM or H:MM. It means some clock today
     }else if (date.match(/^[0-9]?[0-9]:[0-9][0-9]$/g)) {
         let timeOfDay = hhmmToTimestamp(date);
         let thisMorning = moment().startOf('day').unix()
         return (+thisMorning + +timeOfDay + (moment().utcOffset()*60) );

     // if string match signed HH:MM or H:MM ( like +4:20 or -2:00 ) it mean a time offset from current clock of today.
     }else if (date.match(/^(\+|\-)?[0-9]?[0-9]:[0-9][0-9]$/g)) {
           let timeOffset = hhmmToTimestamp(date.substring(1));

         if(date.charAt(0) === '+'){
           return moment().unix().utc() + timeOffset + (moment().utcOffset()*60);
         }else if (date.charAt(0) === '-') {
           return moment().unix().utc() - timeOffset + (moment().utcOffset()*60);
         }
     }

     // if everything else failed, try to parse it with moment()
     let mdate = moment(date);
     if(mdate.isValid()){
         return mdate.unix().valueOf() + (moment().utcOffset()*60)

     }else{
         return false;
     }

 };












function addTimeZoneOffset(timestamp){
    let offset = (moment(0).utcOffset()*60);
    return timestamp + offset
}

function removeTimeZoneOffset(timestamp){
    let offset = (moment(0).utcOffset()*60);
    return timestamp - offset
}

/**
 * This function try to take various time input and transform them to a GMT unix timestamp
 * This function is not designed to parse any delta
 * @function anyDateToTimestamp
 * @param {string,int} a date to parse
 * @returns {int,boolean} a timestamp
 * @deprecated This function have heratic behaviour conserning UTC vs Local time. Use userInputToTimestamp() or avoid using it
 */

var anyDateToTimestamp = (date) => {
    let now = moment().unix(); //Local time is now at greenwich

    if( ! isNaN(date)  && date > 0){ // if already a timestamp
        return date
    } else if( ! isNaN(date)  && date <= 0){ // if it's a running task
            return -1

    }else if (date === 'now') {
        return now

    }else if (date.match(/^[0-9]?[0-9]:[0-9][0-9]$/g)) { // 4:20 ( 4:20 today)
        let timeOfDay = hhmmToTimestamp(date);
        let thisMorning = moment().startOf('day').unix() // loval
        return (+thisMorning + +timeOfDay);

    }else if (date.match(/^(\+|\-)?[0-9]?[0-9]:[0-9][0-9]$/g)) { // +1:30 ( 1:30 later then now. )
          let symbol =  date.charAt(0);
          let timeOffset = hhmmToTimestamp(date.substring(1));

        if(symbol === '+'){
          return now + timeOffset ;
        }else if (symbol === '-') {
          return now - timeOffset ;
        }
    }

    let mdate = moment(date);  //try to parse what ever it is.
    if(mdate.isValid()){
        return mdate.unix().valueOf() + (moment().utcOffset()*60)
    }else{
        return false;
    }
};


const timestampToUtc = (timestamp) =>{

    return +timestamp  + (moment().utcOffset()*60)
}


const hhmmToTimestamp = (hhmm) => {
    let hh_mm = hhmm.split(':');
    if( hh_mm.length !== 2 || hh_mm[1].length > 2 ){
        return false;
    }else{
        return Math.round((hh_mm[0])*3600 + (hh_mm[1])*60 );
    }
};



const deltaToTime = (delta) => {
    if (delta < 0 ){
        return 'Running...';
    }
    let min = Math.round( (delta/60) );

    if( delta < 3600 ){
        return min+' Min';
    }else{
        let hours = Math.floor((delta/60)/60);
        min = min % 60;
        if (min <= 9 ){
            min = '0'+min;
        }
        return hours+':'+min+' Hours';
    }
};



const timestampToHumanDate = (timestamp) => {
    let date = new Date( +timestamp*1000 );
    let year = date.getFullYear();
    let month = ("0"+(date.getMonth()+1)).substr(-2);
    let day = ("0"+date.getDate()).substr(-2);
    return timestampToHumaTime(timestamp)+"   "+year+"-"+month+"-"+day ;
};



const timestampToHumaTime = (timestamp) => {
    let date = new Date( timestamp*1000 );
    let hour = ("0"+date.getHours()).substr(-2);
    let minutes = ("0"+date.getMinutes()).substr(-2);
    return hour+":"+minutes;
};



module.exports = {
    anyDateToTimestamp,
    timestampToHumanDate,
    deltaToTime,
    hhmmToTimestamp,
    timestampToHumaTime,
    timestampToUtc,
    addTimeZoneOffset,
    removeTimeZoneOffset,
    userInputToTimestamp
};
