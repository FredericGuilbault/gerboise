process.env.NODE_ENV = 'test';
const { exec } = require('child_process');
const expect = require('expect');

describe('CLI arguments', () => {


    it('Should return help info on empty arguments.', (done) => {


        exec('./gerboise-cli.js', (err, stdout, stderr) => {
            expect(stdout.trim()).toEqual('-h, --help  Show help');
            done();
        });

    });


    it('Should start a new session.', (done) => {
        exec('./gerboise-cli.js start foo@bar', (err, stdout, stderr) => {
            expect(stdout.trim()).toBe('Session ID 1 have started. Good work !');
            done();
        });

    });



    it('Should see the running session.', (done) => {
        exec('./gerboise-cli.js start foo@bar ; ./gerboise-cli.js running', (err, stdout, stderr) => {
            expect(stdout).toBe(1);
            done();
        });



    });


    it('Stop the session in 30 min', (done) => {
        exec('./gerboise-cli.js stop 1 -t:+0:30:', (err, stdout, stderr) => {
            expect(stdout).toBe("Session stopped\n" + "31 Min\n");
            done();
        });

    });




});

