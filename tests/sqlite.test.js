process.env.NODE_ENV = 'test';
const expect = require('expect');
const db = require('./../sqlite.js');
const staticNow = Date.now();


describe('Database functions', () => {

  describe('Boot sequence', () => {

    it('Should find 5 tables in the database', (done) => {
      expect(JSON.stringify(db.countTables())).toBe("[{\"COUNT(*)\":5}]");
      done()
    });

  });

  describe('Creations', () => {
    it('Should create projectName "project-test123" and return the new id of it.', (done) => {
        expect(db.createProject('project-test123')).toBe(2)
        done()
    });

    it('Should NOT be able to create projectName "project-test123" twice', (done) => {
        expect(db.createProject('project-test123')).toBe(false)
        done()
    });

    it('Should create taskName "task-test123" and return the new id of it.', (done) => {
        expect(db.createTask('task-test123')).toBe(2)
        done()
    });

    it('Should NOT be able to create taskName "task-test123" twice', (done) => {
        expect(db.createTask('task-test123')).toBe(false)
        done()
    });

    it('Should start a new session', (done) => {
      var sessionData = {
          start:staticNow,
          task:2,
          project:2,
      }
      expect(db.createSession(sessionData)).toBe(1)
      done()
    });

  });



  describe('Readings', () => {
      it('Should list all projects', (done) => {
          expect(JSON.stringify(db.getAllProjects()))
              .toBe("[{\"id\":1,\"name\":\"default\"},{\"id\":2,\"name\":\"project-test123\"}]")
          done()
      });

      it('Should return project name when an ID is query', (done) => {
          expect(db.getProjectId(1)).toBe('default')
          done()
      });

      it('Should return project ID when name is query', (done) => {
          expect(db.getProjectName('default')).toBe(1)
          done()
      });


      it('Should list all tasks', (done) => {
          expect(JSON.stringify(db.getAlltasks()))
              .toBe("[{\"id\":1,\"name\":\"default\"},{\"id\":2,\"name\":\"task-test123\"}]")
          done()
      });

      it('Should return task name when an ID is query', (done) => {
          expect(db.getTaskId(1)).toBe('default')
          done()
      });

      it('Should return task ID when name is query', (done) => {
          expect(db.getTaskName('default')).toBe(1)
          done()
      });

      it('Should list all Sessions', (done) => {
          expect(JSON.stringify(db.getAllSessions(false)))
              .toBe("[{\"id\":1,\"start\":\""+staticNow+"\",\"stop\":\"-1\",\"task\":2,\"project\":\"2\",\"comment\":\"\",\"user\":0,\"tags\":\"\"}]")
          done()
      });

      it('Should list all Sessions between', (done) => {
          expect(JSON.stringify(db.getSessionsStartedBetween(0,staticNow+5,false)))
              .toBe("[{\"id\":1,\"start\":\""+staticNow+"\",\"stop\":\"-1\",\"task\":2,\"project\":\"2\",\"comment\":\"\",\"user\":0,\"tags\":\"\"}]")
          done()
      });

      it('Should return session ID 1', (done) => {
          expect(JSON.stringify(db.getSessionId(1)))
              .toBe("{\"id\":1,\"start\":\""+staticNow+"\",\"stop\":\"-1\",\"task\":2,\"project\":\"2\",\"comment\":\"\",\"user\":0,\"tags\":\"\"}")
          done()
      });
  });

  describe('Updates', () => {
      it('Should set session 1 as ended', (done) => {
          var data = { stop:99999999999999}
          expect(JSON.stringify(db.updateSession(1,data))).toBe(undefined)
          expect(JSON.stringify(db.getSessionId(1)))
              .toBe("{\"id\":1,\"start\":\""+staticNow+"\",\"stop\":\"99999999999999\",\"task\":2,\"project\":\"2\",\"comment\":\"\",\"user\":0,\"tags\":\"\"}")
          done()
      });
  });

  describe('Utils', () => {
     it('should add human readable data to sessions', (done) => {

       //  console.log(JSON.stringify(db.addHumanReadableToSession(db.getSessionId(1))))
         let data = {"id":1,"start":9000,"stop":99999999999999,"task":1,"project":1,"comment":"","user":0,"tags":""};
         expect(JSON.stringify(db.addHumanReadableToSession(data ))).toBe(
             "{\"id\":1,\"start\":9000,\"stop\":99999999999999,\"task\":1,\"project\":1,\"comment\":\"\",\"user\":0,\"tags\":\"\",\"delta\":99999999990999,\"taskName\":\"default\",\"projectName\":\"default\",\"taskProject\":\"default@default\"}"
         );
         done();
        //
     })

  })



});
