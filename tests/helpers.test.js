const expect = require('expect');
const helper = require('./../helper.js');


describe('Helper functions', () => {

    it('anyDateToTimestamp : should return the timestamp of "now"', (done) => {
        let utcDate = new Date(new Date().getTime());
        expect(helper.anyDateToTimestamp('now') ).toBeCloseTo(Math.round(utcDate.getTime()  / 1000), 2);
        done();
    });

    it('anyDateToTimestamp : should return the timestamp of differents iso formats', (done) => {
        expect(helper.anyDateToTimestamp('2008-09-15T15:53:00+05:00')).toBe(1221475980);
        expect(helper.anyDateToTimestamp('2008-09-15T15:53:00')).toBe(1221508380);
        done();
    });

    it('anyDateToTimestamp : should return today time at 1:30', (done) => {
        let utcDate = new Date(new Date().getTime());
        let morningTimestamp = utcDate.setHours(0, 0, 0, 0)/1000;

        expect(helper.anyDateToTimestamp('01:30')).toBeCloseTo((morningTimestamp + 5400), 2);
        expect(helper.anyDateToTimestamp('23:00')).toBeCloseTo((morningTimestamp + 82800), 2);
        done();
    });

    it('anyDateToTimestamp : should return now +1:30 or now -1:30', (done) => {
        let utcDate = Math.round(new Date()/1000.0);
        expect(helper.anyDateToTimestamp('+1:30')).toBeCloseTo((utcDate  + 5400), 2);
        expect(helper.anyDateToTimestamp('-1:30')).toBeCloseTo((utcDate  - 5400), 2);
        done();
    });

    it('anyDateToTimestamp : should transform time string to the current timestamp', (done) => {
      expect(helper.anyDateToTimestamp(' 2:10 2018/10/22')).toBe(1540188600);
      expect(helper.anyDateToTimestamp('02:10 2018/10/22')).toBe(1540188600);
      expect(helper.anyDateToTimestamp('02:10 2018/01/01')).toBe(1514790600);
      expect(helper.anyDateToTimestamp('02:10 2018/1/1')).toBe(1514790600);
      expect(helper.anyDateToTimestamp('02:10    2018/10/22')).toBe(1540188600);

      expect(helper.anyDateToTimestamp(1540188600)).toBe(1540188600);
      expect(helper.anyDateToTimestamp(-1)).toBe(-1);
      done();
  });

  it('deltaToTime: should return time as hh:mm of a given timestamp', (done) => {
      expect(helper.deltaToTime('0')).toBe('0 Min');
      expect(helper.deltaToTime('2700')).toBe('45 Min');
      expect(helper.deltaToTime('3600')).toBe('1:00 Hours');
      expect(helper.deltaToTime('3663')).toBe('1:01 Hours');
      expect(helper.deltaToTime('4140')).toBe('1:09 Hours');
      expect(helper.deltaToTime('4200')).toBe('1:10 Hours');
      expect(helper.deltaToTime('4260')).toBe('1:11 Hours');
      expect(helper.deltaToTime('345600')).toBe('96:00 Hours');
      expect(helper.deltaToTime('348600')).toBe('96:50 Hours');
      done()
  });

    it('hhmmToTimestamp: should return a timestamp in seconds of a hh:mm string', (done) => {
        expect(helper.hhmmToTimestamp('01:30')).toBe(5400);
        expect(helper.hhmmToTimestamp('1:30')).toBe(5400);
        expect(helper.hhmmToTimestamp('1:00')).toBe(60*60);
        expect(helper.hhmmToTimestamp('1:0')).toBe(60*60);
        done()
    })
});