# gerboise

A Linux Time tracker that aim to be the successor of projectHamster.

! gerboise is currently at beta RC 1 level. This mean that most of core functions are expected to work but not stable !
Issue report and pull requests are more then welcome.

== How to install ==

`git clone git@gitlab.com:FredericGuilbault/gerboise.git`

`cd gerboise`

`npm install`

To launch Gui form dbus :

`npm install nw -g nw@0.31.0 --unsafe-perm`


==== Launch cli ====

`./gerboise-cli.js [action] [arguments]`

==== Launch GUI ====

`npm start`

`npm start edit [session ID]`

or simply `nw` if you have it installed globally.

=== Manually generate a standalone report ===
`node generate-report.js`

For webserver use (php), Just copy all files under some webroot folder then edit the config.php file to set the hamster's database location.

For dev : In config.php if you set the constant DEV to true. the webserver version will use files in /src/ instead of report.html

At this point the `report.html` file is created manually from a generated file using the DEV constant.

==== Launch dbus ====

It have to be said that gerboise dbus emulate projectHamster interface and methods names.
Therefor it's incompatible with projectHamster's dbus services.

`./gerboise-dbus.js`

or `./gerboise-cli.js -d` to run it as demon

===== Development =====

=== Unit test ===

`npm test`

gerboise don't have full coverage and don't plan to.
Testing is implemented as needed, on devs discretion.
