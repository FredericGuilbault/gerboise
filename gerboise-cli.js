#!/usr/bin/env node
const yargs = require('yargs');
const app = require('./core.js');
const sqlite = require('./sqlite.js');
const helper = require('./helper.js');
const table = require('cli-table');


let argv = yargs.usage('Usage: $0 <command> [options]')
//  .command('Gerboise is a a Linux Time tracker ')
    .command('demon, -d', 'Run as demon with dbus')
    .alias('d', 'demon')
    .command('start [task@project]', 'start a new session.')
    .command('stop [id]', 'stop the provided session ID')
    .alias('task@project', 'tp')

    .command('createProject [name]', 'Create a new project with provided name.')
    .command('createTask [name]', 'Create a new task with provided name.')

    .command('project [name]', 'Get all sessions related to a specific project')
    .command('task [name]', 'Get all sessions related to a specific task')
    .command('taskproject [task@project]', 'Get all sessions for [project@task]')

    .command('running', 'Show running sessions.')
    .command('today', 'Show today sessions')
    .describe('time, -t', 'Force the action to append at a specific time')
    .alias('t', 'time')

    .help('h')
    .alias('h', 'help')
    .epilog('copyright 2018')
    .argv;


if (argv.d) {
    const dbus = require('./dbus');
    let hamsterInterface = new dbus.startHamsterService();
    require('daemonize-process')();


} else if (argv._[0] === 'createProject') {
    app.newProject(argv.name, (id) => {
        if (id) {
            console.log('Project ID ' + id + ' created')
        }
    })


} else if (argv._[0] === 'createTask') {
    app.newTask(argv.name, (id) => {
        if (id) {
            console.log('Task ID ' + id + ' created')
        }
    })


} else if (argv._[0] === 'start') {
    if (!argv.tp) {
        console.log('Plz specify the [task@project] to start.');
        return 0;
    }
    let taskAndProject = argv.tp.split('@');
    if (!taskAndProject[1]) {
        taskAndProject[1] = 'default';
    }

    let start = 'now';
    if (argv.time) {
        start = argv.time
    }

    app.newSession({
        'start': start,
        'task': taskAndProject[0],
        'project': taskAndProject[1]
    }, (id) => {
        if (id) {
            console.log('Session ID ' + id + ' have started. Good work !')
        }
    })


} else if (argv._[0] === 'stop') {
    let stop = 'now';
    if (argv.time) {
        stop = argv.time
    }
    app.stopSession(argv.id, stop, (rtn) => {
        if (rtn === -1) {
            console.log('Session already stopped')
        } else {
            console.log('Session stopped');
            app.getSession(argv.id, (session) => {
                console.log(helper.deltaToTime(+session.stop - +session.start))
            })
        }
    })


} else if (argv._[0] === 'running') {
    app.getRunningSessions((sessions) => {
        if (sessions.length === 0) {
            console.log('0 sessions are open.')
        } else {
            for (let i in sessions) {
                console.log(sessions[i].id)
            }
        }
    })


} else if (argv._[0] === 'today') {
    app.getTodaySessions((sess) => {
        if (sess.length === 0) {
            console.log("Nothing yet.")
        } else {
            printSessionTable(sess)
        }
    })


} else if (argv._[0] === 'project') {
    if (argv.name) {
        app.getSessionsOfProject(argv.name, (sess) => {
            printSessionTable(sess)
        });
    } else {
        app.listProjects((list) => {
            printIdNameTable(list)
        });
    }


} else if (argv._[0] === 'task') {
    if (argv.name) {
        app.getSessionsOfTask(argv.name, (sess) => {
            printSessionTable(sess)
        });
    } else {
        app.listTasks((list) => {
            printIdNameTable(list)
        });
    }


} else if (argv._[0] === 'taskproject') {
    let taskAndProject = argv.tp.split('@');
    app.getSessionsOfTaskAtProject(taskAndProject[0], taskAndProject[1], (sess) => {
        printSessionTable(sess)
    });
} else {
    console.log('  -h, --help  Show help   ');
}


function printSessionTable(sess) {
    let theTable = new table({
        head: ['ID', 'Task', 'Time'],
        colWidths: [6, 30, 20]
    });

    let time, task;
    for (let i in sess) {
        task = sqlite.getTaskId(sess[i].task) + '@' + sqlite.getProjectId(sess[i].project) + '   ';
        if (sess[i].stop == -1) {
            time = 'Running..'
        } else {
            time = helper.deltaToTime(+sess[i].stop - +sess[i].start);
        }
        theTable.push([sess[i].id, task, time]);
    }
    console.log(theTable.toString());
}


function printIdNameTable(array) {
    let theTable = new table({
        head: ['ID', 'Name'],
        colWidths: [6, 30]
    });

    for (let i in array) {
        theTable.push([array[i].id, array[i].name]);
    }
    console.log(theTable.toString());
}